package com.kfstandard.multicalculatorfx.app;


//import com.kfstandard.multicalculatorfx.calculator.Calculations;
//import com.kfstandard.multicalculatorfx.data.FinanceBean;
import com.kfstandard.multicalculatorfx.presentation.LoanCalculatorFX;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 *
 * @author Ken Fogel
 */
public class LoanCalculatorMain extends Application {

    //private Calculations calc;
    //private FinanceBean finance;
    private LoanCalculatorFX gui;

    @Override
    public void init() {
        //calc = new Calculations();
        //finance = new FinanceBean();
        gui = new LoanCalculatorFX(/* calc, finance */);
    }

    @Override
    public void start(Stage primaryStage) {
        gui.start(primaryStage);
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
